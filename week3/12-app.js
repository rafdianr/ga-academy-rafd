console.log("running")

let head = document.getElementById("head")
let query = document.querySelector(".head")
let queryId = document.querySelector("#head")

console.log("id", head)
console.log("query", query)
console.log("queryid", queryId)

let item = document.querySelectorAll(".item")
let byClass = document.getElementsByClassName("item")
let byTag = document.getElementsByTagName("li")

console.log("item", item)
console.log("byClass", byClass)
console.log("byTag", byTag)

//traversal/penelusuran
let ul = document.querySelector(".lists")
let child1 = ul.firstElementChild
let childSibling = child1.nextElementSibling
console.log("ul", child1)
console.log("child1Sib", childSibling)
console.log("parent", child1.parentElement)

let klik = document.querySelector(".btn")

klik.addEventListener('click', changeColor);

function changeColor() {
  klik.style.backgroundColor = "red"
}