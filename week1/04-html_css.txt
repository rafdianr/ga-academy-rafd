HTML
Hypertext Markup Language (HTML) adalah sebuah bahasa markah yang digunakan untuk membuat 
sebuah halaman web, menampilkan berbagai informasi di dalam sebuah penjelajah web internet 
dan pemfrmatan hiperteks sederhana yang ditulis dalam berkas format ASCII agar dapat menghasilkan 
tampilan wujud yang terintegerasi.

Tag-tag yang sering digunakan pada HTML, yaitu:
- <html> = untuk membuat sebuah dokumen html
- <head> = untuk menampung informasi yang digunakan dalam dokumen web
- <title>= untuk memberi judul pada web
- <body> = untuk memberi isi pada dokumen web
- heading <h1>, <h2>, ... , <h6>
- paragraph <p>
- list <ul> untuk unordered list, <ol> untuk ordered list
- img, untuk menambah gambar
- link <a> untuk berpindah kepada link yang dituju
- form
- input

CSS (cascading style sheet)
CSS digunakan sebagai pendamping pada dokumen html untuk memberikan pengubahan2 terhadap isi dokumen.
Dalam file CSS terdapat selector dan declaration. Selector berfungsi untuk menunjuk dokumen yang akan 
dilakukan pengubahan dengan menggunakan elemen tag, class/id atribut. Declaration berisi pengubahan yang 
akan dilakukan terhadap selector yang berisi properti dan value. Pengubahan yang dapat dilakukan dapat 
berupa dimensi, box model, positioning, dan layouting.





