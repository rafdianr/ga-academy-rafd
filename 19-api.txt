API digunakan untuk memudahkan pebuatan fitur yang dapat digunakan dalam beberapa platform

Metode-metode yang digunakan dalam API:
1. Get -> untuk mengambil data
2. Post -> untuk mengirimkan data
3. Put -> untuk mengedit data
4. Del -> untuk menghapus data

Dalam linux untuk melakukan testing API dapat menggunakan aplikasi postman. Aplikasi postman 
tersebut dapat digunakan sesuai dengan dokumentasi yang sudah dibuat sebelum diterapkan menjadi
sebuah program.